package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import org.springframework.boot.autoconfigure.web.ResourceProperties;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spells;
    public ChainSpell(ArrayList<Spell> spell){
        this.spells = spell;
    }

    @Override
    public void undo(){
        for(Spell s:spells)
            s.undo();
    }

    @Override
    public void cast(){
        for(Spell s:spells)
            s.cast();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
