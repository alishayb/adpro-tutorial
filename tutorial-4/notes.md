## Eager Instantiation
Eager instatiation dilakukan dengan menginstansiasi sebuah class bahkan sebelum class tersebut dibutuhkan. Keuntungannya adalah kemungkinan untuk lagging lebih kecil karena instancenya sudah ada di memory. Sedangkan kekurangannya adalah waktu yang dibutuhkan untuk menjalankan aplikasi lebih lama karena seluruhnya butuh terload.

## Lazy Instantiation
Lazy instantiation tidak langsung menginstansiasi class, namun menunggu hingga class tersebut dibutuhkan. Kelebihannya adalah aplikasi yang kita jalankan dapat berjalan lebih cepat, sementara kekurangannya dapat terjadi lagging ketika pertama kali mengakses sesuatu dimana aplikasi perlu membuat instance class yang bersangkutan.