package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public class LiyuanSobaFactory implements MenuFactory{
    public Noodle createNoodle(){
        return new Soba();
    };
    public Meat createMeat() {
        return new Beef();
    };
    public Topping createTopping() {
        return new Mushroom();
    };
    public Flavor createFlavor() {
        return new Sweet();
    };
}
