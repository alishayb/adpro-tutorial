package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MeatTest {
    @Test
    public void testBeef(){
        Beef beef = new Beef();
        assertEquals("Adding Maro Beef Meat...", beef.getDescription());
    }

    @Test
    public void testChicken(){
        Chicken chicken = new Chicken();
        assertEquals("Adding Wintervale Chicken Meat...", chicken.getDescription());
    }

    @Test
    public void testFish(){
        Fish fish = new Fish();
        assertEquals("Adding Zhangyun Salmon Fish Meat...", fish.getDescription());
    }

    @Test
    public void testPork(){
        Pork pork = new Pork();
        assertEquals("Adding Tian Xu Pork Meat...", pork.getDescription());
    }
}
