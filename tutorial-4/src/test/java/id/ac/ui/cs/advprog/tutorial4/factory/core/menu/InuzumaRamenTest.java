package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;

public class InuzumaRamenTest {
    private InuzumaRamen inuzumaRamen;
    @BeforeEach
    public void setUp() throws Exception{
        inuzumaRamen = new InuzumaRamen("Inuzuma Ramen");
    }

    @Test
    public void testInuzumaRamenName(){
        assertEquals("Inuzuma Ramen", inuzumaRamen.getName());
    }

    @Test
    public void testInuzumaRamenNoodleIsCorrect(){
        assertThat(inuzumaRamen.getNoodle(), instanceOf(Ramen.class));
    }

    @Test
    public void testInuzumaRamenMeatIsCorrect(){
        assertThat(inuzumaRamen.getMeat(), instanceOf(Pork.class));
    }

    @Test
    public void testInuzumaRamenToppingIsCorrect(){
        assertThat(inuzumaRamen.getTopping(), instanceOf(BoiledEgg.class));
    }

    @Test
    public void testInuzumaRamenFlavorIsCorrect(){
        assertThat(inuzumaRamen.getFlavor(), instanceOf(Spicy.class));
    }
}
