package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MenuServiceTest {

    private MenuServiceImpl menuService;

    @BeforeEach
    public void setUp(){
        menuService = new MenuServiceImpl();
    }

    @Test
    public void testMenuServiceCreateMenu(){
        menuService.createMenu("Soba", "LiyuanSoba");
        assertEquals("Soba", menuService.getMenus().get(4).getName());
    }
}
