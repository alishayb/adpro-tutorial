package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private MondoUdon mondoUdon;
    @BeforeEach
    public void setUp() throws Exception{
        mondoUdon = new MondoUdon("Mondo Udon");
    }

    @Test
    public void testMondoUdonName(){
        assertEquals("Mondo Udon", mondoUdon.getName());
    }

    @Test
    public void testMondoUdonNoodleIsCorrect(){
        assertThat(mondoUdon.getNoodle(), instanceOf(Udon.class));
    }

    @Test
    public void testMondoUdonMeatIsCorrect(){
        assertThat(mondoUdon.getMeat(), instanceOf(Chicken.class));
    }

    @Test
    public void testMondoUdonToppingIsCorrect(){
        assertThat(mondoUdon.getTopping(), instanceOf(Cheese.class));
    }

    @Test
    public void testMondoUdonFlavorIsCorrect(){
        assertThat(mondoUdon.getFlavor(), instanceOf(Salty.class));
    }
}
