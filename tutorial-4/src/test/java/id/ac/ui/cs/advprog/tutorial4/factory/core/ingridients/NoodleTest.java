package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NoodleTest {
    @Test
    public void testRamen(){
        Ramen ramen = new Ramen();
        assertEquals("Adding Inuzuma Ramen Noodles...", ramen.getDescription());
    }

    @Test
    public void testShirataki(){
        Shirataki shirataki = new Shirataki();
        assertEquals("Adding Snevnezha Shirataki Noodles...", shirataki.getDescription());
    }

    @Test
    public void testSoba(){
        Soba soba = new Soba();
        assertEquals("Adding Liyuan Soba Noodles...", soba.getDescription());
    }

    @Test
    public void testUdon(){
        Udon udon = new Udon();
        assertEquals("Adding Mondo Udon Noodles...", udon.getDescription());
    }
}
