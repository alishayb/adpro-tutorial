package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

public class MenuRepositoryTest {
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp(){
        menuRepository = new MenuRepository();
    }

    @Test
    public void testMenuRepositoryGetList(){
        List<Menu> menuList = menuRepository.getMenus();
        assertThat(menuList, instanceOf(List.class));
    }

    @Test
    public void testMenuRepositoryCanBeAdded(){
        menuRepository.add(new InuzumaRamen("IRamen"));
        List<Menu> menuList = menuRepository.getMenus();
        assertThat(menuList.get(0), instanceOf(InuzumaRamen.class));
    }
}
