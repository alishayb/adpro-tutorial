package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {
    private SnevnezhaShirataki snevnezhaShirataki;
    @BeforeEach
    public void setUp() throws Exception{
        snevnezhaShirataki = new SnevnezhaShirataki("Snevnezha Shirataki");
    }

    @Test
    public void testSnevnezhaShiratakiName(){
        assertEquals("Snevnezha Shirataki", snevnezhaShirataki.getName());
    }

    @Test
    public void testSnevnezhaShiratakiNoodleIsCorrect(){
        assertThat(snevnezhaShirataki.getNoodle(), instanceOf(Shirataki.class));
    }

    @Test
    public void testSnevnezhaShiratakiMeatIsCorrect(){
        assertThat(snevnezhaShirataki.getMeat(), instanceOf(Fish.class));
    }

    @Test
    public void testSnevnezhaShiratakiToppingIsCorrect(){
        assertThat(snevnezhaShirataki.getTopping(), instanceOf(Flower.class));
    }

    @Test
    public void testSnevnezhaShiratakiFlavorIsCorrect(){
        assertThat(snevnezhaShirataki.getFlavor(), instanceOf(Umami.class));
    }
}
