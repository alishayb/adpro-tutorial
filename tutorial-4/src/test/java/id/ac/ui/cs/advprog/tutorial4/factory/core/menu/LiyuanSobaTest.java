package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private LiyuanSoba liyuanSoba;
    @BeforeEach
    public void setUp() throws Exception{
        liyuanSoba = new LiyuanSoba("Liyuan Soba");
    }

    @Test
    public void testLiyuanSobaName(){
        assertEquals("Liyuan Soba", liyuanSoba.getName());
    }

    @Test
    public void testLiyuanSobaNoodleIsCorrect(){
        assertThat(liyuanSoba.getNoodle(), instanceOf(Soba.class));
    }

    @Test
    public void testLiyuanSobaMeatIsCorrect(){
        assertThat(liyuanSoba.getMeat(), instanceOf(Beef.class));
    }

    @Test
    public void testLiyuanSobaToppingIsCorrect(){
        assertThat(liyuanSoba.getTopping(), instanceOf(Mushroom.class));
    }

    @Test
    public void testLiyuanSobaFlavorIsCorrect(){
        assertThat(liyuanSoba.getFlavor(), instanceOf(Sweet.class));
    }
}
