package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceTest {
    private OrderServiceImpl orderService;

    @BeforeEach
    public void setUp(){
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testOrderADrink(){
        orderService.orderADrink("Mango juice");
        assertEquals("Mango juice", orderService.getDrink().toString());
    };

    @Test
    public void testOrderAFood(){
        orderService.orderAFood("Fries");
        assertEquals("Fries", orderService.getFood().toString());
    };
}
