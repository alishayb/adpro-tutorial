package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        //ToDo: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    public void update(){
    	Quest quest = this.guild.getQuest();
    	String type = this.guild.getQuestType();
    	
    	if(type.equals("D") || type.equals("E")) {
    		this.getQuests().add(quest);
    	}
    }
    //ToDo: Complete Me
}
