package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        //ToDo: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    public void update(){
    	this.getQuests().add(this.guild.getQuest());
    }
    
    //ToDo: Complete Me
}
