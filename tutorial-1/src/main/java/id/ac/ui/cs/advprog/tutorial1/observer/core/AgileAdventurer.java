package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        
        //ToDo: Complete Me
        this.guild = guild;
        guild.add(this);
    }

    public void update(){
    	Quest quest = this.guild.getQuest();
    	String type = this.guild.getQuestType();
    	
    	if(type.equals("D") || type.equals("R")) {
    		this.getQuests().add(quest);
    	}
    }
    //ToDo: Complete Me
}
